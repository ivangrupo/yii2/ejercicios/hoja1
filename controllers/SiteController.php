<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Entradas;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionListar(){
        $s = Entradas::find()->asArray()->all();
        return $this->render('listar', [
            'datos' => $s,
        ]);
    }
    
    public function actionListar1(){
        $s = Entradas::find()->all();
        return $this->render('listarTodos', [
            'datos' => $s,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        return $this->render('index');
    }

}
