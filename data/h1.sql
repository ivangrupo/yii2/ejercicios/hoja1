﻿
DROP DATABASE IF EXISTS h1;
CREATE DATABASE h1
	CHARACTER SET latin1
	COLLATE latin1_swedish_ci;


USE h1;

CREATE TABLE entradas (
  id int(11) NOT NULL AUTO_INCREMENT,
  texto varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

INSERT INTO entradas VALUES
  (1, 'Ejemplo de entrada'),
  (2, 'Otra más');
